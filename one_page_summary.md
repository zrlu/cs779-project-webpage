# CS 779 Project: 1-Page Summary

## Base Project

Implement Loop subdivision using half-edge data structure.

## Extras

- Support meshes with boundary.
- Implement the adaptive subdivision based on dihedral angles.
- Fix cracks.
- Experiment with the adapive subdivision and the non-adaptive one and compare the number of primitives generated.
- Subdivide various interesting models.

## What I Learned

- Manipulate half-edge data structure and apply this on Loop subdivision.
- Fix the bad pointers of the boundary vertices resulted from the split and flip operations.
- Understand the reason of cracks and how to fix them.
- Calculate the boundary vertices from the selected faces.
- Use the dihedral angle as a guide for adaptive subdivision.
- Understand the reason of high-valence vertices.
